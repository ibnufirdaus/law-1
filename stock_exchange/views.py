import os

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
import requests
from django.urls import reverse


def index(request):
    response = {}

    if "symbol" in request.POST:
        response = get_info(request)

    return render(request, "index.html", response)


def get_info(request):
    api_key = os.environ.get("ALPHA_VANTAGE_API_KEY")
    symbol = request.POST["symbol"]

    url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&apikey={}".format(symbol, api_key)
    response = requests.get(url=url).json()

    if("Error Message" in response):
        result = {
            "status": "FAILED",
            "message": "Stock information of symbol `{}` is not found".format(symbol),
            "result": {}
        }
    elif("Note" in response):
        result = {
            "status": "FAILED",
            "message": "Stock information of symbol `{}` could not be retrieved due to API calls limitation ".format(symbol)
                       + "(5 per minute). Please try in the next minute",
            "result": {}
        }
    else:
        constructed_response = {
            "symbol": response["Meta Data"]["2. Symbol"],
            "last_updated": response["Meta Data"]["3. Last Refreshed"]
        }

        stock_time_series = response["Time Series (Daily)"]
        latest_stock_time_series = stock_time_series[list(stock_time_series.keys())[0]]
        constructed_response["open"] = latest_stock_time_series["1. open"]
        constructed_response["high"] = latest_stock_time_series["2. high"]
        constructed_response["low"] = latest_stock_time_series["3. low"]
        constructed_response["close"] = latest_stock_time_series["4. close"]
        constructed_response["volume"] = latest_stock_time_series["5. volume"]

        result = {
            "status": "SUCCESS",
            "message": "Stock information of symbol `{}` is found".format(symbol),
            "result": constructed_response
        }

    return result
